import { productApi } from '../gateways/ProductApi';
import history from '../history';

export const REQUEST_PRODUCTS = 'REQUEST_PRODUCTS';
export const RECEIVE_PRODUCTS = 'RECEIVE_PRODUCTS';
export const CREATE_PRODUCT = 'CREATE_PRODUCT';
export const REMOVE_PRODUCT = 'REMOVE_PRODUCT';
export const UPDATE_PRODUCT = 'UPDATE_PRODUCT';

const requestProducts = () => ({
  type: REQUEST_PRODUCTS,
});

const receiveProducts = (json) => ({
  type: RECEIVE_PRODUCTS,
  products: json.map((product) => product),
});

export const fetchProducts = () => (dispatch) => {
  dispatch(requestProducts());
  const json = productApi.getProducts();
  dispatch(receiveProducts(json));
};

export const createProduct = (product) => (dispatch) => {
  dispatch({
    type: CREATE_PRODUCT,
    product,
  });
  history.push('/');
};

export const removeProduct = (productId) => ({
  type: REMOVE_PRODUCT,
  productId,
});

export const updateProduct = (product) => (dispatch) => {
  dispatch({
    type: UPDATE_PRODUCT,
    product,
  });

  history.push('/');
};
