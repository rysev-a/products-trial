import React from 'react';
import PropTypes from 'prop-types';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import moment from 'moment';

import Header from '../../Header/Header';
import { createProduct } from '../../../actions/products';
import { getNewProductId } from '../../../reducers/products';
import ProductForm, { defaultProductState } from './ProductForm';

const ProductCreateContainer = ({ onSubmit, product, categories }) => {
  return (
    <>
      <Header name="Update product" />
      <ProductForm
        onSubmit={onSubmit}
        product={product}
        categories={categories}
      />
    </>
  );
};

ProductCreateContainer.propTypes = {
  onSubmit: PropTypes.func.isRequired,
  product: PropTypes.shape({}).isRequired,
  categories: PropTypes.array.isRequired,
};

const mapStateToProps = (state) => {
  const id = getNewProductId(state);
  const product = {
    ...defaultProductState(id),
    createdAt: moment().format('MM/DD/YYYY hh:mm a'),
  };

  return {
    categories: state.categories,
    product,
  };
};

const mapDispatchToProps = (dispatch) =>
  bindActionCreators(
    { onSubmit: (formState) => createProduct(formState.product) },
    dispatch
  );

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ProductCreateContainer);
