import React, { Component } from 'react';
import { Form, Label, Input, FormGroup, Button, FormText } from 'reactstrap';
import Select from 'react-select';
import { range } from 'lodash';

import validateForm from './utils/validateForm';

const FieldInfo = ({ errorMessage, helpText }) => {
  return errorMessage ? (
    <FormText color="danger">{errorMessage}</FormText>
  ) : (
    <FormText className="form-text text-muted">{helpText}</FormText>
  );
};

export const defaultProductState = (id) => ({
  id,
  name: '',
  categories: '',
  rating: 1,
  brand: '',
});

class ProductForm extends Component {
  constructor(props) {
    super(props);
    this.state = {
      product: props.product,
      touched: props.touched,
      errors: {},
    };
  }

  updateField = (key, value) => {
    const product = { ...this.state.product, [key]: value };
    const errors = validateForm(product);

    this.setState({
      product,
      errors,
      touched: true,
    });
  };

  updateRating = (e) => {
    const { value } = e.target;
    const product = {
      ...this.state.product,
      rating: value,
      featured: Number(value) > 8,
    };

    this.setState({ product });
  };

  isDisableSubmit = () => {
    return Object.keys(this.state.errors).length !== 0 || !this.state.touched;
  };

  onSubmit = (e) => {
    e.preventDefault();
    this.props.onSubmit(this.state);
  };

  onSelectCategories = (categories) => {
    const categoriesIDs = categories
      ? categories.map((category) => {
          return category.id;
        })
      : [];

    this.updateField('categories', categoriesIDs);
  };

  getSelectedCategories = () => {
    return this.props.categories.filter((category) => {
      return this.props.product.categories.indexOf(category.id) !== -1;
    });
  };

  render() {
    const { product, errors } = this.state;

    return (
      <Form>
        <FormGroup>
          <Label>Product name</Label>
          <Input
            onChange={(e) => this.updateField('name', e.target.value)}
            value={product.name}
            placeholder="Enter product name"
            invalid={Boolean(errors.name)}
          />
          <FieldInfo errorMessage={errors.name} helpText="Enter product name" />
        </FormGroup>
        <FormGroup>
          <Label>Product brand</Label>
          <Input
            onChange={(e) => this.updateField('brand', e.target.value)}
            value={product.brand}
            placeholder="Enter product brand"
          />
        </FormGroup>
        <FormGroup>
          <Label>Items in stock</Label>
          <Input
            type="number"
            onChange={(e) => this.updateField('itemsInStock', e.target.value)}
            value={product.itemsInStock}
            placeholder="Enter product count in stock"
            invalid={Boolean(errors.itemsInStock)}
          />
          <FieldInfo
            errorMessage={errors.itemsInStock}
            helpText="Enter product count in stock"
          />
        </FormGroup>
        <FormGroup>
          <Label>Select categories</Label>
          <Select
            options={this.props.categories}
            getOptionLabel={(option) => option.name}
            getOptionValue={(option) => option.id}
            onChange={this.onSelectCategories}
            defaultValue={this.getSelectedCategories()}
            isMulti
          />
          <FieldInfo
            errorMessage={errors.categories}
            helpText="Select from 1 to 5 categories for product"
          />
        </FormGroup>
        <FormGroup>
          <Label for="exampleSelect">Select rating</Label>
          <Input
            type="select"
            defaultValue={product.rating}
            onChange={this.updateRating}
          >
            {range(1, 11).map((rating) => {
              return (
                <option key={rating} value={rating}>
                  {rating}
                </option>
              );
            })}
          </Input>
        </FormGroup>
        <FormGroup>
          <Label>Select expiration date</Label>
          <Input
            type="date"
            invalid={Boolean(this.state.errors.expirationDate)}
            onChange={(e) => {
              this.updateField('expirationDate', e.target.value);
            }}
            defaultValue={product.expirationDate}
          />
          <FieldInfo
            errorMessage={errors.expirationDate}
            helpText="Choose expiration date for product"
          />
        </FormGroup>
        <FormGroup>
          <Label>Select receipt date</Label>
          <Input
            type="date"
            onChange={(e) => {
              this.updateField('receiptDate', e.target.value);
            }}
            defaultValue={product.receiptDate}
          />
        </FormGroup>
        <Button
          type="submit"
          className="btn btn-primary"
          disabled={this.isDisableSubmit()}
          onClick={this.onSubmit}
          color={this.isDisableSubmit() ? 'secondary' : 'success'}
        >
          Submit
        </Button>
      </Form>
    );
  }
}

export default ProductForm;
