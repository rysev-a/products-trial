import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import Header from '../../Header/Header';
import ProductForm from './ProductForm';
import { updateProduct } from '../../../actions/products';
import { getProductsById } from '../../../reducers/products';
import { bindActionCreators } from 'redux';

const ProductUpdateContainer = ({ onSubmit, product, categories }) => {
  return (
    <>
      <Header name="Update product" />
      <ProductForm
        onSubmit={onSubmit}
        product={product}
        categories={categories}
        touched
      />
    </>
  );
};

ProductUpdateContainer.propTypes = {
  onSubmit: PropTypes.func.isRequired,
  product: PropTypes.shape({}).isRequired,
  categories: PropTypes.array.isRequired,
};

const mapStateToProps = (state, props) => {
  const {
    match: {
      params: { id },
    },
  } = props;
  const product = getProductsById(state)[id];

  return {
    product,
    categories: state.categories,
  };
};

const mapDispatchToProps = (dispatch) =>
  bindActionCreators(
    {
      onSubmit: (formState) => updateProduct(formState.product),
    },
    dispatch
  );

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ProductUpdateContainer);
