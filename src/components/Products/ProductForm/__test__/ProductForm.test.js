import React from 'react';
import Enzyme, { mount } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import { range } from 'lodash';
import categoriesMock from '../../../../mocks/categories';
import ProductForm, { defaultProductState } from '../ProductForm';

Enzyme.configure({ adapter: new Adapter() });

const setup = () => {
  const props = {
    onSubmit: jest.fn(),
    product: defaultProductState(1),
    categories: categoriesMock,
  };

  const enzymeWrapper = mount(<ProductForm {...props} touched />);

  return {
    props,
    enzymeWrapper,
  };
};

describe('ProductForm', () => {
  describe('update product name', () => {
    it('should update field', () => {
      const { enzymeWrapper } = setup();
      enzymeWrapper.instance().updateField('name', 'new name');
      expect(enzymeWrapper.state().product.name).toEqual('new name');
    });

    it('should show name is required error', () => {
      const { enzymeWrapper } = setup();
      enzymeWrapper.instance().updateField('name', '');
      expect(enzymeWrapper.state().errors.name).toEqual('Name is required');
    });

    it('should show to long name error', () => {
      const { enzymeWrapper } = setup();
      enzymeWrapper.instance().updateField('name', range(1, 201).join(''));
      expect(enzymeWrapper.state().errors.name).toEqual(
        'Max name length is 200'
      );
    });
  });

  describe('rating', () => {
    it('should update rating with featured true', () => {
      const { enzymeWrapper } = setup();
      enzymeWrapper.instance().updateRating({ target: { value: 10 } });
      expect(enzymeWrapper.state().product.featured).toEqual(true);
    });

    it('should update rating with featured false', () => {
      const { enzymeWrapper } = setup();
      enzymeWrapper.instance().updateRating({ target: { value: 5 } });
      expect(enzymeWrapper.state().product.featured).toEqual(false);
    });
  });

  describe('submit form', () => {
    it('should disable submit', () => {
      const { enzymeWrapper, props } = setup();
      enzymeWrapper.instance().updateField('name', '');
      enzymeWrapper.find('button').simulate('click');
      expect(props.onSubmit.mock.calls.length).toEqual(0);
    });

    it('should submit on button click', () => {
      const { enzymeWrapper, props } = setup();
      enzymeWrapper.find('button').simulate('click');
      expect(props.onSubmit.mock.calls[0]).toEqual([
        {
          product: defaultProductState(1),
          touched: true,
          errors: {},
        },
      ]);
      expect(props.onSubmit.mock.calls.length).toEqual(1);
    });
  });
});
