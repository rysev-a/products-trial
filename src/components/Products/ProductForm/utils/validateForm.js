import moment from 'moment';
const validators = [
  {
    field: 'name',
    errorMessage: 'Name is required',
    validate: (value) => {
      return value;
    },
  },
  {
    field: 'name',
    errorMessage: 'Max name length is 200',
    validate: (value) => {
      return value.length <= 200;
    },
  },
  {
    field: 'categories',
    errorMessage: 'Select from 1 to 5 categories',
    validate: (value) => {
      return value.length < 6;
    },
  },
  {
    field: 'categories',
    errorMessage: 'Select at least one category',
    validate: (value) => {
      return value.length > 0;
    },
  },
  {
    field: 'expirationDate',
    errorMessage:
      'Expiration date it should expire not less than 30 days since now',
    validate: (value) => {
      if (!value) {
        return true;
      }

      const minDate = moment().add(30, 'days');
      const expDate = moment(value, 'YYYY-MM-DD');

      return expDate > minDate;
    },
  },
  {
    field: 'itemsInStock',
    errorMessage: 'Invalid items count',
    validate: (value) => {
      if (value && value < 0) {
        return false;
      }

      return true;
    },
  },
];

const validateForm = (product) => {
  return validators.reduce((errors, validator) => {
    const value = product[validator.field];
    return validator.validate(value)
      ? errors
      : {
          ...errors,
          [validator.field]: validator.errorMessage,
        };
  }, {});
};

export default validateForm;
