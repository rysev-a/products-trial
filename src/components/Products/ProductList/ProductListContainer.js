import React from 'react';
import PropTypes from 'prop-types';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import Header from '../../Header/Header';
import ProductsList from './Products';
import { removeProduct } from '../../../actions/products';
import { getCategoriesById } from '../../../reducers/categories';

const ProductsContainer = ({ products, removeProduct }) => (
  <>
    <Header name="Products" />
    <ProductsList products={products} removeProduct={removeProduct} />
  </>
);

ProductsContainer.propTypes = {
  products: PropTypes.array.isRequired,
  removeProduct: PropTypes.func.isRequired,
};

const mapStateToProps = (state) => {
  const categoriesById = getCategoriesById(state);

  const products = state.products.items.map((product) => {
    const categories = product.categories.map((id) => categoriesById[id]);

    return {
      ...product,
      categories,
    };
  });

  return {
    products,
    isLoaded: state.products.isLoaded,
  };
};

const mapDispatchToProps = (dispatch) =>
  bindActionCreators({ removeProduct }, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(ProductsContainer);
