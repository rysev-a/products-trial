import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { fetchCategories } from '../../../actions/categories';
import { fetchProducts } from '../../../actions/products';

class ProductsDataProvider extends Component {
  componentDidMount() {
    this.props.fetchCategories();
    this.props.fetchProducts();
  }

  render() {
    return this.props.isLoaded ? (
      this.props.children
    ) : (
      <h2>...loading products</h2>
    );
  }
}

ProductsDataProvider.propTypes = {
  fetchProducts: PropTypes.func.isRequired,
  fetchCategories: PropTypes.func.isRequired,
  isLoaded: PropTypes.bool.isRequired,
};

const mapStateToProps = (state) => {
  return {
    isLoaded: state.products.isLoaded,
  };
};

const mapDispatchToProps = (dispatch) =>
  bindActionCreators({ fetchCategories, fetchProducts }, dispatch);

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ProductsDataProvider);
