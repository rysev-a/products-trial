import React from 'react';
import { Navbar, Nav, NavItem } from 'reactstrap';
import { NavLink } from 'react-router-dom';

const TopMenu = () => (
  <Navbar light expand="lg">
    <NavLink className="navbar-brand" to="/">
      Products
    </NavLink>
    <Nav className="mr-auto" navbar>
      <NavItem>
        <NavLink className="nav-link" to="/create">
          Create product
        </NavLink>
      </NavItem>
    </Nav>
  </Navbar>
);

export default TopMenu;
