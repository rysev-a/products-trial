import { categories as reducer } from '../categories';
import * as categoriesActions from '../../actions/categories';

describe('categories reducer', () => {
  it('should return initial state', () => {
    expect(reducer([], { type: 'unknownType' })).toEqual([]);
  });

  it('should recieve data', () => {
    const categoriesData = [{ id: 1, name: 'Apple' }];
    const initialState = [];
    const action = {
      type: categoriesActions.RECEIVE_CATEGORIES,
      categories: categoriesData,
    };

    expect(reducer(initialState, action)).toEqual(categoriesData);
  });
});
