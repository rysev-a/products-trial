import {
  products as reducer,
  defaultState,
  getProductsById,
  getNewProductId,
} from '../products';
import * as productsActions from '../../actions/products';
import products from '../../mocks/products';

describe('products reducer', () => {
  it('should return initial state', () => {
    expect(reducer({}, { type: 'unknownType' })).toEqual({});
  });

  it('should recieve data', () => {
    const initialState = defaultState();
    const action = {
      type: productsActions.RECEIVE_PRODUCTS,
      products,
    };

    expect(reducer(initialState, action)).toEqual({
      isLoaded: true,
      items: products,
    });
  });

  it('should add new product', () => {
    const initialState = {
      isLoaded: true,
      items: products,
    };

    const newProduct = { id: 4, name: 'new product', categories: [1, 2, 3] };
    const action = {
      type: productsActions.CREATE_PRODUCT,
      product: newProduct,
    };

    expect(reducer(initialState, action)).toEqual({
      isLoaded: true,
      items: [...products, newProduct],
    });
  });

  it('should remove product', () => {
    const initialState = {
      isLoaded: true,
      items: [{ id: 1 }, { id: 2 }],
    };

    const action = { type: productsActions.REMOVE_PRODUCT, productId: 1 };

    expect(reducer(initialState, action)).toEqual({
      isLoaded: true,
      items: [{ id: 2 }],
    });
  });

  it('should update product', () => {
    const initialState = {
      isLoaded: true,
      items: [{ id: 1 }, { id: 2 }],
    };

    const action = {
      type: productsActions.UPDATE_PRODUCT,
      product: {
        id: 1,
        name: 'product name',
      },
    };

    expect(reducer(initialState, action)).toEqual({
      isLoaded: true,
      items: [{ id: 1, name: 'product name' }, { id: 2 }],
    });
  });
});

describe('product selectors', () => {
  it('should return products map by id', () => {
    const state = {
      products: {
        items: [
          { id: 1, name: 'product 1' },
          { id: 2, name: 'product 2' },
        ],
      },
    };
    expect(getProductsById(state)).toEqual({
      1: { id: 1, name: 'product 1' },
      2: { id: 2, name: 'product 2' },
    });
  });

  it('should return new product id with empty products', () => {
    const state = {
      products: {
        items: [],
      },
    };

    expect(getNewProductId(state)).toEqual(1);
  });

  it('should return new product id with big ids', () => {
    const state = {
      products: {
        items: [
          { id: 125, name: 'product 1' },
          { id: 24, name: 'product 2' },
        ],
      },
    };

    expect(getNewProductId(state)).toEqual(126);
  });
});
