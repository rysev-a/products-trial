import * as productsActions from '../actions/products';

export const defaultState = () => ({
  items: [],
  isLoaded: false,
});

export function products(state = defaultState(), action) {
  switch (action.type) {
    case productsActions.RECEIVE_PRODUCTS:
      return { isLoaded: true, items: [...action.products] };

    case productsActions.CREATE_PRODUCT:
      return { ...state, items: [...state.items, action.product] };

    case productsActions.REMOVE_PRODUCT:
      return {
        ...state,
        items: state.items.filter((product) => {
          return product.id !== action.productId;
        }),
      };

    case productsActions.UPDATE_PRODUCT:
      return {
        ...state,
        items: state.items.map((product) => {
          if (product.id === action.product.id) {
            return action.product;
          }
          return product;
        }),
      };

    default:
      return state;
  }
}

export function getProductsById(state) {
  return state.products.items.reduce((acc, product) => {
    return {
      ...acc,
      [product.id]: product,
    };
  }, {});
}

export function getNewProductId(state) {
  const ids = state.products.items.map((product) => product.id) || [];
  const max = ids.length ? Math.max(...ids) : 0;
  return max + 1;
}
