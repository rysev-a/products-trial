import React from 'react';
import { Router, Switch, Route } from 'react-router-dom';
import history from './history';

import Main from './components/Main/Main';
import TopMenu from './components/TopMenu/TopMenu';
import ProductsLoader from './components/Products/ProductsLoader/ProductsLoader';
import ProductListContainer from './components/Products/ProductList/ProductListContainer';
import ProductCreateContainer from './components/Products/ProductForm/ProductCreateContainer';
import ProductUpdateContainer from './components/Products/ProductForm/ProductUpdateContainer';
import NotFound from './components/NotFound/NotFound';

export function getRoutes() {
  return (
    <Router history={history}>
      <TopMenu />
      <div className="content">
        <div className="container">
          <ProductsLoader>
            <Main>
              <Switch>
                <Route exact path="/" component={ProductListContainer} />,
                <Route path="/create" component={ProductCreateContainer} />,
                <Route path="/update/:id" component={ProductUpdateContainer} />
                <Route path="*" component={NotFound} />,
              </Switch>
            </Main>
          </ProductsLoader>
        </div>
      </div>
    </Router>
  );
}

export default getRoutes;
